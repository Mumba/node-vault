# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]

## [0.2.1] 9 Mar 2018
- Fix up old typings format.
- Updated deps.

## [0.2.0] 15 Aug 2016
- Removed the `version` option in the constructor. Now require full path for `get` and `put`.

## [0.1.1] 10 Aug 2016
- Added export for `VaultOptions`.

## [0.1.0] 15 Jul 2016
- Initial proof-of-concept release.

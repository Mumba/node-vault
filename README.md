# Mumba Vault

A simple Node.js client for [Vault](https://www.vaultproject.io) by HashiCorp.

## Installation 

```sh
$ npm install mumba-vault
```

## Example

See [demos](./demo)

## Tests

To run the test suite, first install the dependencies, then run `npm test`:

```sh
$ npm install
$ npm test
```

## People

The original author of _Mumba Config_ is [Andrew Eddie](https://gitlab.com/u/aeddie.mumba).

## License

[Apache 2.0](LICENSE.txt)

* * *

&copy; 2016 [Mumba Pty Ltd](http://www.mumba.cloud). All rights reserved.


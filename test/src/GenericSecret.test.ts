/**
 * GenericSecret backend tests.
 *
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

const nock = require('nock');
import * as assert from "assert";
import {GenericSecret} from "../../src/index";

describe('GenericSecret unit tests', () => {
	var instance: GenericSecret;
	var req: any;

	beforeEach(() => {
		instance = new GenericSecret();
		req = nock('http://127.0.0.1:8200');
	});

	afterEach(() => nock.cleanAll());

	it('should get data from a path', () => {
		let token = 'the-token';
		let path = '/v1/secret/path';

		req.get('/v1/secret/path')
			.reply(200, {
				data: true
			});

		return instance.get(token, path)
			.then((res: any) => {
				assert.equal(res.data, true);
			});
	});

	it('should handle not found', () => {
		let token = 'token';
		let path = '/v1/secret/path';

		req.get('/v1/secret/path')
			.reply(200, {
				errors: []
			});

		return instance.get(token, path)
			.then((res: any) => {
				assert.deepEqual(res.errors, []);
			});
	});

	it('should handle put data', () => {
		let token = 'the-token';
		let path = '/v1/secret/path';
		let data = { foo: 'bar' };

		req.put('/v1/secret/path')
			.reply(204);

		return instance.put(token, path, data)
			.then((res: any) => {
				assert.equal(res, void 0);
			});
	});
});

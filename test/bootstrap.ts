/**
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

/**
 * Asserts that a Promise rejects with a similar signature to `assert.throws`.
 *
 * @param {Promise} promise
 * @param {RegExp}  regex
 * @returns {Promise<TResult>|Promise<T>}
 */
export function assertRejects(promise: Promise<any>, regex: RegExp) {
	return promise.then(() => {
			throw new Error('Missing expected rejection.');
		})
		.catch((err: Error) => {
			if (!regex.test(err.message)) {
				throw err;
			}
		});
}

/**
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

const request = require('request');

export interface VaultOptions {
	host?: string;
	port?: number;
}

/**
 * Generic Secret support for Vault.
 */
export class GenericSecret {
	private host: string;
	private port: number;

	/**
	 * Constructor
	 *
	 * @param {VaultOptions} options
	 */
	constructor(options: VaultOptions = {}) {
		this.host = options.host || 'http://127.0.0.1';
		this.port = options.port || 8200;
	}

	private getUri() {
		return [this.host, ':', this.port].join('');
	}

	private send(token: string, method: string, path: string, data?: any) {
		let headers: any = {
			'X-Vault-Token': token
		};

		if (method == 'post' || method == 'put') {
			headers['Content-Type'] = 'application/x-www-form-urlencoded';
		}

		return new Promise((resolve, reject) => {
			request[method]({
				uri: this.getUri() + path,
				headers: headers,
				json: data
			}, (err: Error, res: any, body: string) => {
				if (err) {
					return reject(err);
				}

				if (!body) {
					return resolve();
				}

				try {
					let json = JSON.parse(body);

					if (json.errors && json.errors.length > 0) {
						reject(new Error(json.errors.join('\n')));
					}
					else {
						resolve(json)
					}
				}
				catch (e) {
					reject(e);
				}
			});
		});
	}

	/**
	 * Get a value from Vault.
	 *
	 * @param {string} token - A Vault authentication token.
	 * @param {string} path  - The path.
	 * @returns {Promise<object>}
	 */
	public get(token: string, path: string): Promise<any> {
		return this.send(token, 'get', path);
	}

	/**
	 * Put a value into Vault.
	 *
	 * @param {string} token - A Vault authentication token.
	 * @param {string} path  - The path.
	 * @param {object} data  - The data.
	 * @returns {Promise<object>}
	 */
	public put(token: string, path: string, data: any): Promise<any> {
		return this.send(token, 'put', path, data);
	}
}

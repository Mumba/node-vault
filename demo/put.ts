/**
 * @copyright 2016 Mumba Pty Ltd. All rights reserved.
 * @license   Apache-2.0
 */

import {GenericSecret} from "../src/GenericSecret";

let vault = new GenericSecret();
let token = process.argv[2];

console.log('PUTTING DATA');
vault.put(token, '/v1/secret/path', { foo: 'bar' })
	.then(() => {
		console.log('PUT SUCCESSFUL - GETTING DATA');
		return vault.get(token, '/v1/secret/path');
	})
	.then((res: any) => {
		console.log('GOT:', JSON.stringify(res.data, null, '  '));
	})
	.catch(console.error);
